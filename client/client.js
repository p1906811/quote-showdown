/* ******************************************************************
 * Constantes de configuration
 */
const apiKey = "377fcb5e-cf43-4e1b-bb84-1262a7aa9672";

// http://localhost:3000
// https://lifap5.univ-lyon1.fr
const serverUrl = "https://lifap5.univ-lyon1.fr";

/* ******************************************************************
 * Gestion des tabs "Voter" et "Toutes les citations"
 ******************************************************************** */

/**
 * Récupère les citations du serveur (copié de whomai).
 * @returns les citations sous forme de données JSON.
 */
function getQuotesFromServer()
{
  console.log('CALL getQuotesFromServer');
  return fetch(serverUrl + "/citations", { headers: { "x-api-key": apiKey } })
  .then((response) => response.json())
  .then((jsonData) => {
    if (jsonData.status && Number(jsonData.status) != 200) {
      return { err: jsonData.message };
    }
    return jsonData;
  })
  .catch((erreur) => ({ err: erreur }));
}

/**
 * Transforme une citation donnée en HTML pur.
 *  
 * @param {Citation} quote la citation à parser.
 * @returns du HTML pur.
 */
function rowifyQuote(quote)
{
  return `<tr><td></td><td>${quote.character}</td><td>${quote.quote}</td></tr>`;
}

/**
 * Transforme une liste de citations en HTML pur.
 *  
 * @param {Citations} quotes la liste de citations à parser.
 * @returns un bloc de HTML.
 */
function blobifyQuotes(quotes)
{
  return quotes.map(quote => rowifyQuote(quote)).join('');
}

/**
 * Ajoute les citations (en JSON) au tableau.
 * 
 * @param {Citations} quotes les citations à transformer.
 * 
 */
function addHTMLToTable(quotes)
{
  // Fonction qui ajoute le bloc de HTML contenant les citations au tableau
  function addBlobToTable(newHTML)
  {
    document.querySelector('table').innerHTML += newHTML;
  }
  // Convertit les citations sous forme JSON en HTML parsé
  addBlobToTable(blobifyQuotes(quotes));
}

/**
 * Ajoute les citations (en HTML) au tableau.
 * 
 * @param {Citations} quotes la citation à parser.
 * 
 */
function regenTableHTML(quotes)
{
  const table = document.querySelector('table');
  quotes.forEach(quote => {
    table.appendChild(quote);
  });
}

/**
 * Génère le squelette du tableau "Toutes les citations"
 */
function generateTable()
{
  // Sélectionne le tableau "toutes les citations" dans le document HTMl
  const table = document.querySelector("table");
  
  // Génère l'entête et la ligne contenant les titres des colonnes
  const thead = table.createTHead();
  const trow = thead.insertRow(0);

  // Colonne "Classement"
  const rankTH = document.createElement('th');
  rankTH.innerHTML = '<span class=""></span> <a id="txtr">Classement</a>';
  trow.appendChild(rankTH);
  // Colonne "Personnage"
  const charTH = document.createElement('th');
  charTH.innerHTML = '<span class=""></span> <a id="txtp">Personnage</a> <br> <input type="text" placeholder="Filtre" oninput="filterTable(1, this.value)"> ';
  trow.appendChild(charTH);

  // Colonne "Citation"
  const quoteTH = document.createElement('th');
  quoteTH.innerHTML = '<span class=""></span> <a id="txtq">Citation</a> <br> <input type="text" placeholder="Filtre" oninput="filterTable(2, this.value)"> ';
  trow.appendChild(quoteTH);
}

/**
 * Initialise les gestionnaires d'évènement 
 * sur les entêtes pour le filtrage
 * 
*/
function listenToHeaders() {
  console.log("CALL listenToHeaders");
  document.getElementById("txtr").onclick = () => registerHeaderClick(0);
  document.getElementById("txtp").onclick = () => registerHeaderClick(1);
  document.getElementById("txtq").onclick = () => registerHeaderClick(2);
}

/**
 * Renvoie deux citations aléatoires et 
 * différentes entre elles.
 * 
 * @param {Citations} quotes La liste de citations
 * @returns deux citations aléatoires
*/
function getQuoteDuo(quotes)
{
  const randIndex1 = Math.round(Math.random()*quotes.length);
  const randomQuote1 = quotes[randIndex1];
  const randIndex2 = Math.round(Math.random()*(quotes.length - 1));
  const randomQuote2 = quotes.filter(
    (quote, index) => (index != randIndex1))[randIndex2];
  return [randomQuote1, randomQuote2];
}

/**
 * Affiche un duel de citations
 * 
 * @param {Citations} quoteDuo Le duo de citations
*/
function showDuelists(quoteDuo)
{
  const leftImg = document.getElementById('leftimg');
  const rightImg = document.getElementById('rightimg');
  const leftQuote = document.getElementById('leftquote');
  const rightQuote = document.getElementById('rightquote');
  const leftChar = document.getElementById('leftchar');
  const rightChar = document.getElementById('rightchar');

  if(quoteDuo[0].characterDirection == "Right")
  {
    leftImg.innerHTML = `<img src="${quoteDuo[0].image}" style="transform: scaleX(-1)"/>`;
  }
  else { leftImg.innerHTML = `<img src="${quoteDuo[0].image}"/>`; }

  if(quoteDuo[1].characterDirection == "Left")
  {
    rightImg.innerHTML = `<img src="${quoteDuo[1].image}" style="transform: scaleX(-1)"/>`;
  }
  else { rightImg.innerHTML = `<img src="${quoteDuo[1].image}"/>`; }

  leftQuote.innerHTML = `${quoteDuo[0].quote}`;
  rightQuote.innerHTML = `${quoteDuo[1].quote}`;
  leftChar.innerHTML = `${quoteDuo[0].character}`;
  rightChar.innerHTML = `${quoteDuo[1].character}`;
}


/**
 * Génère et envoie les résultats du duel au serveur.
 * @param {Citation} winner la citation gagnante
 * @param {Ciattion} loser la citation perdante
 */
function win(winner, loser)
{
  // C'est "loser" et pas "looser", mais ce n'est pas grave
  const duelResult = { 
    "winner": winner._id,
    "looser": loser._id
  };;
  fetch('https://lifap5.univ-lyon1.fr/citations/duels', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', "x-api-key": apiKey},
    body: JSON.stringify(duelResult)
  });
}

/**
 * Démarre un duel.
 * @param {Citations} quotes la liste de citations
 * @returns le duo de citations participant au duel
 */
function startDuel(quotes)
{
  const duo = getQuoteDuo(quotes, [], 0);
  showDuelists(duo);
  return duo;
}

/**
 * Met en place l'écoute des boutons de vote.
 * @param {Citations} quotes la liste de citations
 */
function initVoteButtons(quotes)
{
  document.getElementById("leftbutton").addEventListener('click', function() {
    const duo = startDuel(quotes);
    win(duo[0], duo[1]);
  });
  document.getElementById("rightbutton").addEventListener('click', function() {
    const duo = startDuel(quotes);
    win(duo[1], duo[0]);
  });
}

/**
 * Change la visibilité des lignes du tableau
 * selon la correspondance au critère indiqué
 *
 * @param {Index} index Le numéro de la colonne sur laquelle opérer le filtre
 * @param {Critère} criteria le filtre entré par l'utilisateur
*/
function filterTable(index, criteria)
{
  // Récupère les lignes du tableau sous forme de liste
  const rows = Array.prototype.slice.call(document.querySelector("table").rows);

  // Change la visibilité indépendamment de la casse.
  rows.slice(1).forEach(row => {
    if(!row.cells[index].innerHTML.toLowerCase().includes(criteria.toLowerCase()))
    {
      row.style.visibility = "collapse"; 
    }
    else
    {
      row.style.visibility = "visible";
    } 
  });
}

/**
 * Trie la table selon un ordre spécifié
 * @param {Index} index Le numéro de la colonne sur laquelle opérer le tri
 * @param {Critère} order L'ordre choisi
 * @returns une liste de citations triées (sous forme d'objets HTML).
*/
function sortTable(index, order)
{
  const table = document.querySelector("table");
  const rows = Array.prototype.slice.call(table.rows);
  rows.shift();
  if(order == 'ascending')
  {
    return rows.sort(function(a, b) { 
      return a.children[index].innerHTML.toString().localeCompare(b.children[index].innerHTML.toString());
    });  
  }
  else if(order == 'descending')
  {
    return rows.sort(function(b, a) { 
      return a.children[index].innerHTML.toString().localeCompare(b.children[index].innerHTML.toString());
    });  
  }
  else
  {
    return rows.sort(function(b, a) { 
      return Math.round(Math.random());
    });
  }
}


function regenSortedTable(index, order)
{
  const table = document.querySelector('table');
  const sortedQuotes = sortTable(index, order, []);
  table.innerHTML = '';
  generateTable();
  listenToHeaders();
  regenTableHTML(sortedQuotes);
}

function updateSortingOrderTick(index, char)
{
  const HTMLheaderCells = document.querySelector('table').rows[0].cells;
  const arrHeaderCells = Array.prototype.slice.call(HTMLheaderCells);

  const clickedHeader = arrHeaderCells[index];
  clickedHeader.childNodes[0].innerHTML = char;
}

function registerHeaderClick(index)
{
  // On récupère les entêtes dans une liste
  const HTMLheaderCells = document.querySelector('table').rows[0].cells;
  const arrHeaderCells = Array.prototype.slice.call(HTMLheaderCells);

  // On récupère le header sur lequel on a cliqué
  const clickedHeader = arrHeaderCells[index];
  // On remet les tags de triage des autres colonnes à zéro
  arrHeaderCells.splice(index, 1).forEach(header => header.style.class = '');

  // Tri du tableau en fonction de son état actuel
  // Ascending => Descending
  // Descending => Aléatoire
  // Aléatoire => Ascending
  if(clickedHeader.childNodes[0].innerHTML.includes('^'))
  {
    regenSortedTable(index, 'descending');
    updateSortingOrderTick(index, 'v');
  }
  else if(clickedHeader.childNodes[0].innerHTML.includes('v'))
  {
    regenSortedTable(index, 'random');
    updateSortingOrderTick(index, ' ');
  }
  else
  {
    regenSortedTable(index, 'ascending');
    updateSortingOrderTick(index, '^');
  }
}

/**
 * Affiche/masque les divs "div-duel" et "div-tout"
 * selon le tab indiqué dans l'état courant.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majTab(etatCourant) {
  console.log("CALL majTab");
  const dDuel = document.getElementById("div-duel");
  const dTout = document.getElementById("div-tout");
  const dAjout = document.getElementById("div-ajout");
  const tDuel = document.getElementById("tab-duel");
  const tTout = document.getElementById("tab-tout");
  const tAjout = document.getElementById("tab-ajout");

  if (etatCourant.tab === "duel") {
    dDuel.style.display = "flex";
    tDuel.classList.add("is-active");
    dTout.style.display = "none";
    tTout.classList.remove("is-active");
    dAjout.style.display = "none";
    tAjout.classList.remove("is-active");
  } else if(etatCourant.tab === "tout") {
    dTout.style.display = "flex";
    tTout.classList.add("is-active");
    dDuel.style.display = "none";
    tDuel.classList.remove("is-active");
    dAjout.style.display = "none";
    tAjout.classList.remove("is-active");
  } else
  {
    dTout.style.display = "none";
    tTout.classList.remove("is-active");
    dDuel.style.display = "none";
    tDuel.classList.remove("is-active");
    dAjout.style.display = "flex";
    tAjout.classList.add("is-active");
  }
}

/**
 * Mets au besoin à jour l'état courant lors d'un click sur un tab.
 * En cas de mise à jour, déclenche une mise à jour de la page.
 *
 * @param {String} tab le nom du tab qui a été cliqué
 * @param {Etat} etatCourant l'état courant
 */
function clickTab(tab, etatCourant) {
  console.log(`CALL clickTab(${tab},...)`);
  if (etatCourant.tab !== tab) {
    etatCourant.tab = tab;
    majPage(etatCourant);
  }
}

/**
 * Enregistre les fonctions à utiliser lorsque l'on clique
 * sur un des tabs.
 *
 * @param {Etat} etatCourant l'état courant
 */
function registerTabClick(etatCourant) {
  console.log("CALL registerTabClick");
  document.getElementById("tab-duel").onclick = () =>
    clickTab("duel", etatCourant);
    document.getElementById("tab-tout").onclick = () =>
    clickTab("tout", etatCourant);  
    document.getElementById("tab-ajout").onclick = () =>
    clickTab("ajout", etatCourant);
}

/* ******************************************************************
 * Gestion de la boîte de dialogue (a.k.a. modal) d'affichage de
 * l'utilisateur.
 * ****************************************************************** */

/**
 * Fait une requête GET authentifiée sur /whoami
 * @returns une promesse du login utilisateur ou du message d'erreur
 */
function fetchWhoami() {
  return fetch(serverUrl + "/whoami", { headers: { "x-api-key": apiKey } })
    .then((response) => response.json())
    .then((jsonData) => {
      if (jsonData.status && Number(jsonData.status) != 200) {
        return { err: jsonData.message };
      }
      return jsonData;
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Fait une requête sur le serveur et insère le login dans
 * la modale d'affichage de l'utilisateur.
 *
 * @param {Etat} etatCourant l'état courant
 * @returns Une promesse de mise à jour
 */
function lanceWhoamiEtInsereLogin(etatCourant) {
  return fetchWhoami().then((data) => {
    etatCourant.login = data.login; // qui vaut undefined en cas d'erreur
    etatCourant.errLogin = data.err; // qui vaut undefined si tout va bien
    majPage(etatCourant);
    // Une promesse doit renvoyer une valeur, mais celle-ci n'est pas importante
    // ici car la valeur de cette promesse n'est pas utilisée. On renvoie
    // arbitrairement true
    return true;
  });
}

/**
 * Affiche ou masque la fenêtre modale de login en fonction de l'état courant.
 * Change la valeur du texte affiché en fonction de l'état
 *
 * @param {Etat} etatCourant l'état courant
 */
function majModalLogin(etatCourant) {
  const modalClasses = document.getElementById("mdl-login").classList;
  if (etatCourant.loginModal) {
    modalClasses.add("is-active");
    const elt = document.getElementById("elt-affichage-login");

    const ok = etatCourant.login !== undefined;
    if (!ok) {
      elt.innerHTML = `<span class="is-error">${etatCourant.errLogin}</span>`;
    } else {
      elt.innerHTML = `Bonjour ${etatCourant.login}.`;
    }
  } else {
    modalClasses.remove("is-active");
  }
}

/**
 * Déclenche l'affichage de la boîte de dialogue du nom de l'utilisateur.
 * @param {Etat} etatCourant
 */
function clickFermeModalLogin(etatCourant) {
  etatCourant.loginModal = false;
  majPage(etatCourant);
}

/**
 * Déclenche la fermeture de la boîte de dialogue du nom de l'utilisateur.
 * @param {Etat} etatCourant
 */
function clickOuvreModalLogin(etatCourant) {
  etatCourant.loginModal = true;
  lanceWhoamiEtInsereLogin(etatCourant);
  majPage(etatCourant);
}

/**
 * Enregistre les actions à effectuer lors d'un click sur les boutons
 * d'ouverture/fermeture de la boîte de dialogue affichant l'utilisateur.
 * @param {Etat} etatCourant
 */
function registerLoginModalClick(etatCourant) {
  document.getElementById("btn-close-login-modal1").onclick = () =>
    clickFermeModalLogin(etatCourant);
  document.getElementById("btn-close-login-modal2").onclick = () =>
    clickFermeModalLogin(etatCourant);
  document.getElementById("btn-open-login-modal").onclick = () =>
    clickOuvreModalLogin(etatCourant);
}


/* ******************************************************************
 * Ajout d'une citation
 */

function getFormValues()
{
  const quote = document.getElementById("quote").value;
  const character = document.getElementById("character").value;
  const image = document.getElementById("image").value;
  const charDirection = document.querySelector('input[name="charDirection"]:checked').value;
  const origin = document.getElementById("origin").value;
  if(document.getElementById("origin").value !== '')
  {
    return {
      "quote" : quote,
        "character" : character,
      "image" : image,
      "characterDirection" : charDirection,
      "origin" : origin
    };
  }
  else
  {
    return {
      "quote" : quote,
        "character" : character,
      "image" : image,
      "characterDirection" : charDirection,
    };
  }
}

function sendData() {
  const object = getFormValues();
  fetch('https://lifap5.univ-lyon1.fr/citations', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', "x-api-key": apiKey},
    body: JSON.stringify(object)
  });
}


/* ******************************************************************
 * Initialisation de la page et fonction de mise à jour
 * globale de la page.
 * ****************************************************************** */

/**
 * Mets à jour la page (contenu et événements) en fonction d'un nouvel état.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majPage(etatCourant) {
  console.log("CALL majPage");
  majTab(etatCourant);
  majModalLogin(etatCourant);
  registerTabClick(etatCourant);
  registerLoginModalClick(etatCourant);
}

/**
 * Appelé après le chargement de la page.
 * Met en place la mécanique de gestion des événements
 * en lançant la mise à jour de la page à partir d'un état initial.
 */
function initClientCitations() {
  console.log("CALL initClientCitations");
  const etatInitial = {
    tab: "duel",
    loginModal: false,
    login: undefined,
    errLogin: undefined,
  };
  majPage(etatInitial);
}

// Appel des fonctions initiales au chargement de la page
document.addEventListener("DOMContentLoaded", () => {
  console.log("Exécution du code après chargement de la page");
  initClientCitations();
  const quotePromise = getQuotesFromServer();
  quotePromise.then((quotes) => {
    generateTable();
    addHTMLToTable(quotes);
    initVoteButtons(quotes);
    startDuel(quotes);
    listenToHeaders();
  });

  // const form = document.getElementById('quoteform');
  // form.addEventListener("submit", function (event) {
  //   event.preventDefault();
  //   sendData();
  // });
  const form = document.forms[0];

form.addEventListener("submit", function(event) {
  event.preventDefault();
  const formData = new FormData(this);
  sendData(formData);
});

});

/** ANTISECHE PRESENTATION 4MN IL FAUT ALLER VITE
 * Affichage de l'ensemble des citations du serveur : generateTable           |    2
 * Affichage d'un duel aléatoire: startDuel                                   |   +4
 * Vote: initVoteButtons et win                                               |   +4
 * Filtre de citation: filterTable et generateTable                           |   +2
 * Tri du tableau des citations: registerHeaderClicks et regenSortedTable     |   +4
 * Ajout d'une citation: sendData                                             |   +4
 *                                                                            | = 20/20
 */