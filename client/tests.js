const apiKey = "377fcb5e-cf43-4e1b-bb84-1262a7aa9672";

const serverUrl = "https://lifap5.univ-lyon1.fr";

document.createElement("table");

const exampleQuotes = [
  {
    "_id": "6058567c999f8d1643f84c08",
    "quote": "Hello, Simpson.",
    "character": "Principal Skinner",
    "image": "https://cdn.glitch.com/3c3ffadc-3406-4440-bb95-d40ec8fcde72%2FSeymourSkinner.png?1497567511460",
    "characterDirection": "Right",
    "scores": {
      "6058567c999f8d1643f84c06": {
        "wins": 1,
        "looses": 0
      }
    }
  },
  {
    "_id": "6058567c999f8d1643f84c06",
    "quote": "They taste like...burning.",
    "character": "Ralph Wiggum",
    "image": "https://cdn.glitch.com/3c3ffadc-3406-4440-bb95-d40ec8fcde72%2FRalphWiggum.png?1497567511523",
    "characterDirection": "Left",
    "scores": {
      "6058567c999f8d1643f84c08": {
        "wins": 0,
        "looses": 1
      }
    }
  },
  {
    "quote": "J'ai connu une polonaise.",
    "character": "Fernand Naudin",
    "image": "https://media.senscritique.com/media/000019334382/960/Les_Tontons_flingueurs.jpg",
    "characterDirection": "Right",
    "origin": "Les tontons flingueurs",
    "addedBy": "p132456789",
    "scores": {
      "6059c14d617ca98251240091": {
        "wins": 5,
        "looses": 2
      },
      "6058567c999f8d1643f84c14": {
        "wins": 0,
        "looses": 3
      }
    },
    "_id": "6058567c999f8d1643f84c08"
  }
]

function fetchWhoami() {
  return fetch(serverUrl + "/whoami", { headers: { "x-api-key": apiKey } })
    .then((response) => response.json())
    .then((jsonData) => {
      if (jsonData.status && Number(jsonData.status) != 200) {
        return { err: jsonData.message };
      }
      return jsonData;
    })
    .catch((erreur) => ({ err: erreur }));
}

function rowifyQuote(quote)
{
  return `<tr><td></td><td>${quote.character}</td><td>${quote.quote}</td></tr>`;
}

function blobifyQuotes(quotes)
{
  return quotes.map(quote => rowifyQuote(quote)).join('');
}

function regenTableHTML(quotes)
{
  const table = document.querySelector('table');
  quotes.forEach(quote => {
    table.appendChild(quote);
  });
}

function generateTable()
{
  const table = document.querySelector("table");
  const thead = table.createTHead();
  const trow = thead.insertRow(0);

  const rankTH = document.createElement('th');
  rankTH.innerHTML = '<span class=""></span> <a id="txtr">Classement</a>';
  trow.appendChild(rankTH);
  const charTH = document.createElement('th');
  charTH.innerHTML = '<span class=""></span> <a id="txtp">Personnage</a> <br> <input type="text" placeholder="Filtre" oninput="filterTable(1, this.value)"> ';
  trow.appendChild(charTH);

  const quoteTH = document.createElement('th');
  quoteTH.innerHTML = '<span class=""></span> <a id="txtq">Citation</a> <br> <input type="text" placeholder="Filtre" oninput="filterTable(2, this.value)"> ';
  trow.appendChild(quoteTH);
}

suite("Test pour fetchWhoami", function() {
  test("On vérifie que les numéros étudiants correspondent",
      function() {
        const resultat_attendu = "p1906811";
        fetchWhoami().then(data => {
          chai.assert.deepEqual(data.login, resultat_attendu);
        });
      });
  });

suite("Test pour blobifyQuote", function() {
  test("On vérifie que le retour soit un bloc HTML au bon format",
      function() {
        const resultat_attendu = `<tr><td></td><td>Principal Skinner</td><td>Hello, Simpson.</td></tr><tr><td></td><td>Ralph Wiggum</td><td>They taste like...burning.</td></tr><tr><td></td><td>Fernand Naudin</td><td>J\'ai connu une polonaise.</td></tr>`;
        chai.assert.deepEqual(blobifyQuotes(exampleQuotes), resultat_attendu);
      });
  });

  suite("Test pour rowifyQuote", function() {
    test("On vérifie que le retour soit une ligne HTML au bon format",
        function() {
          const resultat_attendu = `<tr><td></td><td>Principal Skinner</td><td>Hello, Simpson.</td></tr>`;
          chai.assert.deepEqual(rowifyQuote(exampleQuotes[0]), resultat_attendu);
        });
    });